#!/usr/bin/env python
#coding:utf-8

import os
import sys
import json
import argparse
import imageio
import textwrap
import collections
#import xml.etree.ElementTree as ET
from lxml import etree as ET

from xml.dom import minidom
from pprint import pprint
# svg -> image conversion
import cairosvg

# html creation
import dominate
from dominate.tags import *
from dominate.util import raw

from PIL import Image

try:
    import svgwrite
    from svgwrite import mm
    from svgwrite.container import Group
except:
    log.error("could not find module svgwrite. Install it with: pip install svgwrite")
    quit()

import logging as log
    
# defaults
resources_root_dirname = 'resources/icons'
image_types = {
    "small": {"dirname": "smallschematics", "width": 15},
    "large": {"dirname": "largeschematics", "width": 35}
}
# eighth of mm
# because rotation argument must be integers
scale_factor = 8

# radius of the light (mm * scale_factor)
light_radius = 1 * scale_factor

# distance between light borders (mm * scale_factor)
light_y_distance = 1 * scale_factor

# distance between light groups (mm * scale_factor)
group_y_distance = 1 * scale_factor

# width of the surrounding white border (mm * scale_factor)
stroke_width = 0.5 * scale_factor
stroke_offset = stroke_width/2.0

list_xml_appearance_basename = list()
xml_root_dirname = 'xml/signals'

def gen_frame(path):
    # https://stackoverflow.com/questions/46850318/transparent-background-in-gif-using-python-imageio
    im = Image.open(path)
    alpha = im.getchannel('A')

    # Convert the image into P mode but only use 255 colors in the palette out of 256
    im = im.convert('RGB').convert('P', palette=Image.ADAPTIVE, colors=255)

    # Set all pixel values below 128 to 255 , and the rest to 0
    mask = Image.eval(alpha, lambda a: 255 if a <=128 else 0)

    # Paste the color of index 255 and use alpha as a mask
    im.paste(255, mask)

    # The transparency index is 255
    im.info['transparency'] = 255

    return im

class System(object):

    def __init__(self, json_path):
        # load json file
        with open(json_path) as data_file:    
            self.system = json.load(data_file, object_pairs_hook=collections.OrderedDict)

    def generate_mast_svg_path(self, mast):
        """
        Generate
        resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>.svg
        """
        system_dirname = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])

        number_lights = self.get_number_lights(mast)

        svg_mast_dirname = os.path.join(resources_root_dirname, 'svg/aspects', system_dirname)
        if not os.path.exists(svg_mast_dirname):
            os.makedirs(svg_mast_dirname)
        svg_mast_basename = "mast_%s_%d%s_%s.svg" % (mast['type'], number_lights, mast['suffix'], mast['height'])

        svg_mast_path = os.path.join(svg_mast_dirname, svg_mast_basename)

        return svg_mast_path
    
    def generate_mast_image_path(self, imagetype, mast):
        """
        Generate
        resources/icons/<imagetype_dirname>/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>.svg
        """
        system_dirname = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])

        number_lights = self.get_number_lights(mast)

        imagetype_dirname = image_types[imagetype]['dirname']

        image_mast_dirname = os.path.join(resources_root_dirname, imagetype_dirname, 'aspects', system_dirname)
        if not os.path.exists(image_mast_dirname):
            os.makedirs(image_mast_dirname)
        image_mast_basename = "mast_%s_%d%s_%s.png" % (mast['type'], number_lights, mast['suffix'], mast['height'])

        image_mast_path = os.path.join(image_mast_dirname, image_mast_basename)

        return image_mast_path
        
        
    def svg_render_mast(self, mast, path):
        """
        """
        # init svg drawing
        svg_width, svg_height = self.svg_get_total_dimensions(mast)
        dwg = svgwrite.Drawing(path, profile='full', size=(svg_width*mm, svg_height*mm))
        # set default units as half mm
        dwg.viewbox(0,0,svg_width*scale_factor,svg_height*scale_factor)

        # generate each group of lights
        group_offset = 0
        group_id = 0
        for group in mast['groups']:
            group_id_str = "light_group_%d" % group_id
            offset = self.svg_light_group(dwg, group_id_str, group, group_offset, None, None)
            group_offset = offset + group_y_distance
            group_id = group_id + 1

        # save svg to file
        dwg.save(pretty=True)
        
    def resources_create_mast(self, mast):
        """
        Generate svg, png, gif file for a mast with all its lights activated
        resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>.svg
        """
        log.info("  [resources_create_mast] {0}".format(mast['suffix']))
        svg_mast_path = self.generate_mast_svg_path(mast)
        self.svg_render_mast(mast, svg_mast_path)

        # convert to png
        for imagetype in image_types.keys():
            width = image_types[imagetype]['width']
            image_mast_path = self.generate_mast_image_path(imagetype, mast)
            self.convert_svg_to_png(svg_mast_path, width, image_mast_path)

            # convert to gif
        
    def resources_create_masts(self):
        """
        Generate all svg files for all masts (all lights activated)
        """
        log.info("[resources_create_masts]")
        for mast in self.system['masts']:
            self.resources_create_mast(mast)

    def get_number_lights(self, mast):
        """
        Get the number of active lights. Black does not count.
        """
        groups = mast['groups']
        
        # all positions: with light and without light ("black")
        number_colours = sum([len(group) for group in groups])
        # positions with no light
        number_blacks = sum([group.count("black") for group in groups])

        # lights
        number_lights = number_colours - number_blacks
        
        return number_lights
            
    def generate_mast_aspect_image_path(self, mast, aspect, imagetype, imageformat, flash=None):
        """
        Generate the path for image file
        resources/icons/<imagetype_dirname>/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>/rule-<rule_number>.<gif|png|jpeg>
        """
        system_dirname = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])
        number_lights = self.get_number_lights(mast)
        
        mast_dirname = "{0}-{1}{2}-{3}".format(mast['type'], number_lights, mast['suffix'], mast['height'])
        
        imagetype_dirname = image_types[imagetype]['dirname']
        
        image_mast_aspect_dirname = os.path.join(resources_root_dirname, imagetype_dirname, 'aspects', system_dirname, mast_dirname)
        if not os.path.exists(image_mast_aspect_dirname):
            os.makedirs(image_mast_aspect_dirname)

        # flash suffix
        if flash is None:
            flash_suffix = ''
        elif flash is False:
            flash_suffix = '_0'
        elif flash is True:
            flash_suffix = '_1'

        image_mast_aspect_basename = "rule-{0}{1}.{2}".format(aspect['rule'], flash_suffix, imageformat)

        image_mast_aspect_path = os.path.join(image_mast_aspect_dirname, image_mast_aspect_basename)

        return image_mast_aspect_path

    def generate_mast_aspect_svg_path(self, mast, aspect, flash=None):
        """
        Generate the path for svg file
        resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>/rule-<rule_number>.<gif|png|jpeg>
        """
        system_dirname = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])
        number_lights = self.get_number_lights(mast)
        
        mast_dirname = "{0}-{1}{2}-{3}".format(mast['type'], number_lights, mast['suffix'], mast['height'])
        
        svg_mast_aspect_dirname = os.path.join(resources_root_dirname, 'svg/aspects', system_dirname, mast_dirname)
        if not os.path.exists(svg_mast_aspect_dirname):
            os.makedirs(svg_mast_aspect_dirname)

        # flash suffix
        if flash is None:
            flash_suffix = ''
        elif flash is False:
            flash_suffix = '_0'
        elif flash is True:
            flash_suffix = '_1'

        svg_mast_aspect_basename = "mast_{0}_{1}{2}_{3}-aspect_{4}{5}.svg".format(mast['type'], number_lights, mast['suffix'], mast['height'], aspect['rule'], flash_suffix)

        svg_mast_aspect_path = os.path.join(svg_mast_aspect_dirname, svg_mast_aspect_basename)

        return svg_mast_aspect_path


    def aspect_is_available(self, mast, aspect):
        """
        Return whether this mast can show this aspect
        """
        is_able = False

        # list of available ligths in this mast
        available_lights = [item for sublist in mast['groups'] for item in sublist]

        # list of needed lights to represent this aspect (without flash suffix)
        needed_lights = [i.replace("flash","") for i in aspect['lit']]
        
        if set(needed_lights) <= set(available_lights):
            is_able = True
        #print("  available_lights: %s -- needed_lights: %s -> %s" % (available_lights, needed_lights, is_able))
        
        return is_able

    def svg_get_total_dimensions(self, mast):
        """
        Get total dimensions in mm
        """

        svg_width = 2*(light_radius + light_y_distance) + stroke_width
        
        svg_height = 0
        for group in mast['groups']:
            svg_height += light_y_distance
            number_lights = len(group)
            svg_height += number_lights * (light_radius*2 + light_y_distance)

        number_groups = len(mast['groups'])
        svg_height += (number_groups-1) * group_y_distance + stroke_width

        # convert: fraction of mm -> mm
        svg_width = svg_width / scale_factor
        svg_height = svg_height / scale_factor
    
        log.debug("[svg_get_total_dimensions] svg_width:  {0} mm".format(svg_width))
        log.debug("[svg_get_total_dimensions] svg_height: {0} mm".format(svg_height))

        return svg_width, svg_height

    def svg_light_group(self, dwg, group_id, colours, offset, lit=None, flash=None):
        """
        Generate svg file for a group of lights in a mast. Optional parameter lit defines an aspect.
        Flash flag:
        - None: render flashing as colour with rays
        - True: colours that flashes are on
        - False: colours that flashes are off
        """
        colour_translation = {
            "lunar": "white"
        }

        number_lights = len(colours)
        #print "-- number_lights: %d" % number_lights

        y_offset = light_radius + light_y_distance + offset
        y_delta = 2*light_radius + light_y_distance
        x_centre = light_radius + light_y_distance + stroke_offset

        rect_width = 2*(light_radius + light_y_distance)
        rect_height = number_lights * (2*light_radius + light_y_distance) + light_y_distance
        #print "rect_height: %d" % rect_height

        # create group
        g = Group(id=group_id)

        # background rectangle
        r = dwg.rect(insert=(0+stroke_offset,offset+stroke_offset),
                     size=(rect_width,rect_height),
                     rx=(light_radius+light_y_distance), ry=(light_radius+light_y_distance),
                     stroke='white', stroke_width=stroke_width)
        g.add(r)


        if lit:
            # list of colours that should lit, without flash prefix
            lit_no_flash = [i.replace("flash","") for i in lit]
            log.debug("rendering an aspect: lit={0} lit_no_flash={1}".format(lit, lit_no_flash))
            rendering_aspect = True
        else:
            log.debug("not rendering an aspect")
            rendering_aspect = False

        # lights
        light = 0
        for colour in colours:
            render_rays = False
            y_centre = y_offset + light*y_delta + stroke_offset
            
            # get fill_colour
            if rendering_aspect:
                # rendering an aspect
                if colour=='black':
                    # this position has no light
                    log.debug("  black colour")
                    fill_colour = colour
                elif colour in lit_no_flash:
                    # colour is activated (flashing or not)
                    log.debug("  colour {} is activated in this aspect".format(colour))
                    if colour in lit:
                        # colour is not flashy
                        log.debug("    colour {} is not flashy".format(colour))
                        if colour in colour_translation.keys():
                            fill_colour = colour_translation[colour]
                        else:
                            fill_colour = colour   
                    else:
                        # colour is flashy
                        log.debug("    colour {} is flashy".format(colour))
                        if flash is None:
                            log.debug("      colour {} is flashy and it should be rendered with rays".format(colour))
                            render_rays = True
                            if colour in colour_translation.keys():
                                fill_colour = colour_translation[colour]
                            else:
                                fill_colour = colour                              
                        elif flash:
                            # light is on
                            log.debug("      colour {} is flashy and it should be rendered on".format(colour))
                            if colour in colour_translation.keys():
                                fill_colour = colour_translation[colour]
                            else:
                                fill_colour = colour                              
                        else:
                            # light is off
                            log.debug("      colour {} is flashy and it should be rendered off".format(colour))
                            fill_colour = "darkslategray"
                else:
                    # colour is not activated
                    log.debug("  colour {} is not activated in this aspect".format(colour))
                    fill_colour = "darkslategray"
            else:
                # not rendering an aspect
                if colour in colour_translation.keys():
                    fill_colour = colour_translation[colour]
                else:
                    fill_colour = colour   
            
            # render light
            if render_rays:
                # render light with rays
                l = Group()

                # circle
                c = dwg.circle( center=(x_centre,y_centre), r=light_radius, fill=fill_colour)
                l.add(c)

                # rays
                ray_length = 0.5 * scale_factor # (mm * scale_factor)
                ray_width = 0.2 * scale_factor # (mm * scale_factor)
                for angle in range(0, 360, 45):
                    #r = dwg.rect(insert=(x_centre,(y_centre+light_radius)), size=(0.1,ray_length), fill=fill_colour, transform="rotate(%d,%d,%d)"%(angle, x_centre, y_centre))
                    r = dwg.line(start=(x_centre, y_centre+light_radius),
                                 end=(x_centre, y_centre+light_radius+ray_length),
                                 stroke=fill_colour, stroke_width=ray_width,
                                 transform="rotate(%d,%d,%d)"%(angle, x_centre, y_centre))
                    l.add(r)
            else:
                # render light without rays
                l = dwg.circle( center=(x_centre,y_centre), r=light_radius, fill=fill_colour)

            g.add(l)
            light = light + 1
            
        dwg.add(g)

        offset = rect_height
        return offset




#         # lights
#         light = 0
#         for colour in colours:
#             is_flash = False
#             y_centre = y_offset + light*y_delta + stroke_offset
# 
# 
#             # TODO: review FF8A (red should not be off when flash is False)
#             if colour=="black" or not lit or (lit and (colour in lit_no_flash)):
#                 # light has no colour (black), or
#                 # light has colour and it should be on (because no aspect is being rendered), or
#                 # light has colour and it should be on (according to aspect), provided that is should be on according to flash flag (if set)
#                 
#                 if lit:
#                     # rendering an aspect
#                     
#                     if colour in lit:
#                         # colour has not flash prefix in lit
#                         log.debug("%s: no flash" % colour)
#                         is_flash = False
#                         if colour in colour_translation.keys():
#                             fill_colour = colour_translation[colour]
#                         else:
#                             fill_colour = colour             
#                     else:
#                         # colour has flash prefix in lit
#                         log.debug("%s: flash" % colour)
#                         is_flash = True
#                         if flash is None:
#                             if colour in colour_translation.keys():
#                                 fill_colour = colour_translation[colour]
#                             else:
#                                 fill_colour = colour
#                             
#                         
#                 else:
#                     # not rendering an aspect
#                     
#                     if colour in colour_translation.keys():
#                         fill_colour = colour_translation[colour]
#                     else:
#                         fill_colour = colour
#             else:
#                 # light has colour and it should be off (according to aspect)
#                 fill_colour = "darkslategray"            
# 
#             # render light
#             if is_flash:
#                 if flash is None:
#                     # render light with rays
#                     l = Group()
# 
#                     # circle
#                     c = dwg.circle( center=(x_centre,y_centre), r=light_radius, fill=fill_colour)
#                     l.add(c)
# 
#                     # rays
#                     ray_length = 0.5 * scale_factor # (mm * scale_factor)
#                     ray_width = 0.2 * scale_factor # (mm * scale_factor)
#                     for angle in range(0, 360, 45):
#                         #r = dwg.rect(insert=(x_centre,(y_centre+light_radius)), size=(0.1,ray_length), fill=fill_colour, transform="rotate(%d,%d,%d)"%(angle, x_centre, y_centre))
#                         r = dwg.line(start=(x_centre, y_centre+light_radius),
#                                      end=(x_centre, y_centre+light_radius+ray_length),
#                                      stroke=fill_colour, stroke_width=ray_width,
#                                      transform="rotate(%d,%d,%d)"%(angle, x_centre, y_centre))
#                         l.add(r)
#                 else:
#                     # render light without rays
#                     l = dwg.circle( center=(x_centre,y_centre), r=light_radius, fill=fill_colour)
#             else:
#                 l = dwg.circle( center=(x_centre,y_centre), r=light_radius, fill=fill_colour)
#                 
#             g.add(l)
#             light = light + 1
# 
#         dwg.add(g)
# 
#         offset = rect_height
#         return offset

    
    def svg_render_mast_aspect(self, mast, aspect, path, flash=None):
        """
        flash:
          - None: flash is rendered as rays
          - True: flashing light is lit
          - False: flashing light is unlit
        """
        log.debug("    [svg_render_mast_aspect] mast: {0} aspect: {1} path: {2} flash: {3}".format(mast['suffix'], aspect['rule'], path, flash))

        # init svg drawing
        svg_width, svg_height = self.svg_get_total_dimensions(mast)
        dwg = svgwrite.Drawing(path, profile='full', size=(svg_width*mm, svg_height*mm))
        # set default units as mm
        dwg.viewbox(0,0,svg_width*scale_factor,svg_height*scale_factor)

        # generate each group of lights
        group_offset = 0
        group_id = 0
        for group in mast['groups']:
            group_id_str = "light_group_%d" % group_id
            offset = self.svg_light_group(dwg, group_id_str, group, group_offset, aspect['lit'], flash)
            group_offset = offset + group_y_distance
            group_id = group_id + 1
            
        # save svg to file
        dwg.save(pretty=True)
        
    def convert_svg_to_png(self, svg_path, width, png_path):
        """
        Convert svg to png image, with specified width
        """
        log.debug("    [convert_svg_to_png] {0} -> ({1}) {2}".format(svg_path, width, png_path))
        cairosvg.svg2png(url=svg_path, output_width=width, write_to=png_path)


    def convert_pngs_to_animated_gif(self, png_path_list, gif_path):
        """
        Convert several png images to a single animated gif
        """
        log.debug("    [convert_pngs_to_animated_gif] {} -> {}".format(png_path_list, gif_path))
        
        first_image = gen_frame(png_path_list[0])
        
        images = []
        for png_path in png_path_list[1:]:
            im = gen_frame(png_path)
            images.append(im)
        first_image.save(gif_path, save_all=True, append_images=images, loop=0, duration=600)

    def convert_pngs_to_animated_gif_imageio(self, png_path_list, gif_path):
        """
        Convert several png images to a single animated gif
        """
        log.debug("    [convert_pngs_to_animated_gif] {} -> {}".format(png_path_list, gif_path))
        
        images = []
        for png_path in png_path_list:
            images.append(imageio.imread(png_path))
        imageio.mimsave(gif_path, images, duration=0.6)        
        
    def resources_create_mast_aspect(self, mast, aspect):
        """
        Generate svg, png, gif file for a mast with lights activated for a given aspect
        resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>/rule-<rule_number>.<gif|png|jpeg>
        """
        log.info("  [resources_create_mast_aspect] {0} {1}".format(mast['suffix'], aspect['rule']))
        log.debug("    {}".format(aspect['lit']))

        # whether at least one light must flash for this aspecte
        flashy_lights = [x for x in aspect['lit'] if 'flash' in x]
        has_flash = (len(flashy_lights) != 0)

        # two-state render (for flash)
        if has_flash:
            for flash in [False, True]:
                # svg
                svg_mast_aspect_path = self.generate_mast_aspect_svg_path(mast, aspect, flash)
                self.svg_render_mast_aspect(mast, aspect, svg_mast_aspect_path, flash)
                # png
                for imagetype in image_types.keys():
                    width = image_types[imagetype]['width']
                    png_mast_aspect_path = self.generate_mast_aspect_image_path(mast, aspect, imagetype, 'png', flash)
                    log.debug("    {}".format(png_mast_aspect_path))
                    self.convert_svg_to_png(svg_mast_aspect_path, width, png_mast_aspect_path)
            # animated gif with both states
            for imagetype in image_types.keys():
                gif_mast_aspect_path = self.generate_mast_aspect_image_path(mast, aspect, imagetype, 'gif', None)
                log.debug("    {}".format(gif_mast_aspect_path))
                png_mast_aspect_path_list = []
                for flash in [False, True]:
                    png_mast_aspect_path = self.generate_mast_aspect_image_path(mast, aspect, imagetype, 'png', flash)
                    png_mast_aspect_path_list.append(png_mast_aspect_path)
                self.convert_pngs_to_animated_gif(png_mast_aspect_path_list, gif_mast_aspect_path)

        # static render
        flash = None
        # svg
        svg_mast_aspect_path = self.generate_mast_aspect_svg_path(mast, aspect, flash)
        self.svg_render_mast_aspect(mast, aspect, svg_mast_aspect_path, flash)
        # png
        for imagetype in image_types.keys():
            width = image_types[imagetype]['width']
            png_mast_aspect_path = self.generate_mast_aspect_image_path(mast, aspect, imagetype, 'png', flash)
            log.debug("    {}".format(png_mast_aspect_path))
            self.convert_svg_to_png(svg_mast_aspect_path, width, png_mast_aspect_path)
            # gif (if animated gif was not generated)
            if not has_flash:
                gif_mast_aspect_path = self.generate_mast_aspect_image_path(mast, aspect, imagetype, 'gif', None)
                log.debug("    {}".format(gif_mast_aspect_path))
                self.convert_pngs_to_animated_gif([png_mast_aspect_path], gif_mast_aspect_path)
                    
    def resources_create_masts_aspects(self):
        """
        Generate all svg files for all masts in all aspects (if able)
        """
        log.info("[resources_create_masts_aspects]")
        for mast in self.system['masts']:
            for aspect in self.system['aspects']:
                if self.aspect_is_available(mast, aspect):
                    self.resources_create_mast_aspect(mast, aspect)
                    
    def prettify(self, elem):
        """
        Return a pretty-printed XML string for the Element.
        """
        rough_string = ET.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")

    def generate_xml_appearance_basename(self, mast):
        """
        """
        system_name_year = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])
        system_dirname = system_name_year
        number_lights = self.get_number_lights(mast)
        xml_appearance_dirname = os.path.join(xml_root_dirname, system_dirname)
        xml_appearance_basename = "appearance-%s_%d%s_%s.xml" % (mast['type'], number_lights, mast['suffix'], mast['height'])

        return xml_appearance_basename
        
    def xml_create_appearance(self, mast):
        """
        Generate xml file for a given mast, with all the aspects it can show 
        xml/signals/<system_name>/appearance_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.xml
        """
        #log.info("  [xml_create_appearance] {0}".format(mast['suffix']))
        
        aspects = self.system['aspects']
        system_name_year = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])
        system_dirname = system_name_year

        number_lights = self.get_number_lights(mast)

        xml_appearance_dirname = os.path.join(xml_root_dirname, system_dirname)
        if not os.path.exists(xml_appearance_dirname):
            os.makedirs(xml_appearance_dirname)

        xml_appearance_basename = "appearance-%s_%d%s_%s.xml" % (mast['type'], number_lights, mast['suffix'], mast['height'])
        xml_appearance_path = os.path.join(xml_appearance_dirname, xml_appearance_basename)    

        log.info("  [xml_create_appearance] {0} {1}".format(mast['suffix'], xml_appearance_path))

        NS = "http://www.w3.org/2001/XMLSchema-instance"
        location_attribute = '{%s}noNameSpaceSchemaLocation' % NS

        appearancetable = ET.Element('appearancetable', attrib={location_attribute: "http://jmri.org/xml/schema/appearancetable.xsd"})

        # copyright
        copyright = ET.SubElement(appearancetable, 'copyright')
        copyright.set('xmlns','http://docbook.org/ns/docbook')
        for year in self.system['copyright']['years']:
            year_el = ET.SubElement(copyright, 'year')
            year_el.text = "{0:d}".format(year) 
        holder = ET.SubElement(copyright, 'holder')
        holder.text = self.system['copyright']['holder']

        # authorgroup
        authorgroup =  ET.SubElement(appearancetable, 'authorgroup')
        authorgroup.set('xmlns','http://docbook.org/ns/docbook')
        for author in self.system['authorgroup']['authors']:
            author_el = ET.SubElement(authorgroup, 'author')
            personname_el = ET.SubElement(author_el, 'personname')
            firstname_el =  ET.SubElement(personname_el, 'firstname')
            firstname_el.text = author['personname']['firstname']
            surname_el = ET.SubElement(personname_el, 'surname')
            surname_el.text = author['personname']['surname']
            email_el = ET.SubElement(author_el, 'email')
            email_el.text = author['email']

        # revhistory
        revhistory =  ET.SubElement(appearancetable, 'revhistory')
        revhistory.set('xmlns','http://docbook.org/ns/docbook')
        for revision in self.system['revhistory']['revisions']:
            revision_el = ET.SubElement(revhistory, 'revision')
            revnumber_el = ET.SubElement(revision_el, 'revnumber')
            revnumber_el.text = "{0:d}".format(revision['revnumber'])
            date_el = ET.SubElement(revision_el, 'date')
            date_el.text = revision['date']
            authorinitials_el = ET.SubElement(revision_el, 'authorinitials')
            authorinitials_el.text = revision['authorinitials']
            revremark_el = ET.SubElement(revision_el, 'revremark')
            revremark_el.text = revision['revremark']

        # aspecttable
        aspecttable = ET.SubElement(appearancetable, 'aspecttable')
        aspecttable.text = system_name_year

        # name
        name = ET.SubElement(appearancetable, 'name')
        name.text = mast['name']

        # reference
        reference = ET.SubElement(appearancetable, 'reference')
        reference.text = mast['reference']

        # description
        description = ET.SubElement(appearancetable, 'description')
        description.text = mast['description']

        # appearances
        appearances = ET.SubElement(appearancetable, 'appearances')
        for aspect in aspects:
            if self.aspect_is_available(mast, aspect):
                aspect_element = ET.SubElement(appearances, 'appearance')

                aspectname = ET.SubElement(aspect_element, 'aspectname')
                aspectname.text = aspect['name']

                for show in aspect['lit']:
                    show_element = ET.SubElement(aspect_element, 'show')
                    show_element.text = show

                reference = ET.SubElement(aspect_element, 'reference')
                reference.text = aspect['rule']

                for imagetype in image_types.keys():
                    imagelink = ET.SubElement(aspect_element, 'imagelink')
                    imagelink.text = os.path.join( '../../..', self.generate_mast_aspect_image_path(mast, aspect, imagetype, 'png') )
                    imagelink.set('type',imagetype)

        appearancetable.addprevious(ET.PI('xml-stylesheet', 'type="text/xsl" href="../../XSLT/appearancetable.xsl"'))

        tree = ET.ElementTree(appearancetable)
        tree.write(xml_appearance_path, pretty_print=True, xml_declaration=True, encoding="utf-8")

                    
        # appearancetable_pretty = self.prettify(appearancetable)
        # with open(xml_appearance_path, "w") as f:
        #     #f.write(appearancetable_pretty.encode('utf-8'))
        #     f.write(appearancetable_pretty)

        return xml_appearance_basename
        
    def xml_create_appearances(self):
        """
        Generate all xml files for all masts
        """
        log.info("[xml_create_appearances]")
        for mast in self.system['masts']:
            xml_appearance_basename = self.xml_create_appearance(mast)
            list_xml_appearance_basename.append(xml_appearance_basename)

        #print(list_xml_appearance_basename)
        
    def xml_create_aspects(self):
        """
        Generate a single xml file with all aspects
        xml/signals/<system_name>/aspects.xml
        """
        
        system_name_year = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])    
        system_dirname = system_name_year

        xml_aspects_dirname = os.path.join(xml_root_dirname, system_dirname)
        if not os.path.exists(xml_aspects_dirname):
            os.makedirs(xml_aspects_dirname)

        xml_aspects_basename = "aspects.xml"
        xml_aspects_path = os.path.join(xml_aspects_dirname, xml_aspects_basename)    

        log.info("[xml_create_aspects] {}".format(xml_aspects_path))

        NS = "http://www.w3.org/2001/XMLSchema-instance"
        location_attribute = '{%s}noNameSpaceSchemaLocation' % NS
        aspecttable = ET.Element('aspecttable', attrib={location_attribute: "http://jmri.org/xml/schema/aspecttable.xsd"})


        # name
        name = ET.SubElement(aspecttable, 'name')
        name.text = self.system['name']

        # date
        date = ET.SubElement(aspecttable, 'date')
        date.text = "%d" % self.system['year']

        # reference
        reference = ET.SubElement(aspecttable, 'reference')
        reference.text = self.system['reference']

        # copyright
        copyright = ET.SubElement(aspecttable, 'copyright')
        copyright.set('xmlns','http://docbook.org/ns/docbook')
        for year in self.system['copyright']['years']:
            year_el = ET.SubElement(copyright, 'year')
            year_el.text = "{0:d}".format(year) 
        holder = ET.SubElement(copyright, 'holder')
        holder.text = self.system['copyright']['holder']

        # authorgroup
        authorgroup =  ET.SubElement(aspecttable, 'authorgroup')
        authorgroup.set('xmlns','http://docbook.org/ns/docbook')
        for author in self.system['authorgroup']['authors']:
            author_el = ET.SubElement(authorgroup, 'author')
            personname_el = ET.SubElement(author_el, 'personname')
            firstname_el =  ET.SubElement(personname_el, 'firstname')
            firstname_el.text = author['personname']['firstname']
            surname_el = ET.SubElement(personname_el, 'surname')
            surname_el.text = author['personname']['surname']
            email_el = ET.SubElement(author_el, 'email')
            email_el.text = author['email']

        # revhistory
        revhistory =  ET.SubElement(aspecttable, 'revhistory')
        revhistory.set('xmlns','http://docbook.org/ns/docbook')
        for revision in self.system['revhistory']['revisions']:
            revision_el = ET.SubElement(revhistory, 'revision')
            revnumber_el = ET.SubElement(revision_el, 'revnumber')
            revnumber_el.text = "{0:d}".format(revision['revnumber'])
            date_el = ET.SubElement(revision_el, 'date')
            date_el.text = revision['date']
            authorinitials_el = ET.SubElement(revision_el, 'authorinitials')
            authorinitials_el.text = revision['authorinitials']
            revremark_el = ET.SubElement(revision_el, 'revremark')
            revremark_el.text = revision['revremark']

        # aspects
        aspects = ET.SubElement(aspecttable, 'aspects')
        for aspect in self.system['aspects']:
            aspect_element = ET.SubElement(aspects, 'aspect')

            # copy all elements, except those declared in ignored_keys
            ignored_keys = ['lit',]
            for (key, value) in aspect.items():
                if key not in ignored_keys:
                    element = ET.SubElement(aspect_element, key)
                    element.text = aspect[key]

        #imagetypes
        imagetypes = ET.SubElement(aspecttable, 'imagetypes')
        for type in image_types.keys():
            imagetype_element = ET.SubElement(imagetypes, 'imagetype')
            imagetype_element.set('type', type)

        #appearancefiles
        appearancefiles = ET.SubElement(aspecttable, 'appearancefiles')
        for appearancefile in list_xml_appearance_basename:
            appearancefile_element = ET.SubElement(appearancefiles, 'appearancefile')
            appearancefile_element.set('href',appearancefile)

        aspecttable.addprevious(ET.PI('xml-stylesheet', 'type="text/xsl" href="../../XSLT/aspecttable.xsl"'))

        tree = ET.ElementTree(aspecttable)
        tree.write(xml_aspects_path, pretty_print=True, xml_declaration=True, encoding="utf-8")

            
        # #print ET.tostring(top)
        # aspecttable_pretty = self.prettify(aspecttable)
        # with open(xml_aspects_path, "w") as f:
        #     #f.write(aspecttable_pretty.encode('utf-8'))
        #     f.write(aspecttable_pretty)

        #tree = ET.ElementTree(aspecttable)
        #tree.write(open(xml_aspects_path, 'w'), encoding='utf-8')

    def html_create_index(self):
        """
        Generate a single html file with general information about the system
        xml/signals/<system_name>/index.shtml
        """
        
        system_name_year = "{0:s}-{1:d}".format(self.system['name'], self.system['year'])    
        system_dirname = system_name_year

        html_index_dirname = os.path.join(xml_root_dirname, system_dirname)
        if not os.path.exists(html_index_dirname):
            os.makedirs(html_index_dirname)

        html_index_basename = "index.shtml"
        html_index_path = os.path.join(html_index_dirname, html_index_basename)    

        log.info("[html_create_index] {}".format(html_index_path))

        doc = dominate.document(title="JMRI: {0} - {1}".format(self.system['name'], self.system['year']))
        with doc.head:
            meta(charset=u'utf-8')
        with doc:
            h1("JMRI: {0} - {1}".format(self.system['name'], self.system['year']))
            p(self.system['reference'])
            p1 = p("See the ")
            with p1:
                a("aspect page", href='aspects.xml')
            p1.add(".")

            p2 = p("Signal Mast definitions:")
            with p2:
                ul1 = ul()
                with ul1:
                    for mast in self.system['masts']:
                        item = li()
                        item.add(a(mast['name'], href=self.generate_xml_appearance_basename(mast)))
                
        
        #print(str(doc))
        
        with open(html_index_path, "w") as f:
            f.write(doc.render())
        
    def create_system(self):
        """
        Generate all svg and xml files
        """
        # several svg/image files for all masts
        self.resources_create_masts()
    
        # several svg/image files for all masts
        self.resources_create_masts_aspects()
        
        # several xml files for all masts (appearance_<mast>.xml)
        self.xml_create_appearances()
        
        # one xml file for aspects (aspects.xml)
        self.xml_create_aspects()

        # one html file for the system
        self.html_create_index()
        
def main():
    # arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''\
                                     Generate files for JMRI signals: http://jmri.org/help/en/html/tools/signaling/DefineNewAspects.shtml

                                     Created directories and files:
                                     - xml/signals/<system_name>-<year>/
                                         - aspects.xml
                                         - appearance-<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.xml
                                     - resources/icons/
                                         - (not used by jmri) svg/aspects/<system_name>-<year>/
                                             - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.svg
                                             - <CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>/
                                                 - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>-aspect_<aspect>.svg
                                         - <largeschematics|smallschematics>/aspects/<system_name>-<year>/
                                             - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.png
                                             - <CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>/
                                                 - rule-<aspect>.png
                                     '''))
    parser.add_argument('json_file', 
                        help='json file (see examples in json/ dir)')
    parser.add_argument('-v', '--verbose', action="count", 
                        help="increase output verbosity (e.g., -vv is more than -v)")
    args = parser.parse_args()

    # init system
    s = System(args.json_file)

    # log
    # default level is WARNING
    if args.verbose:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.DEBUG)
        log.info("Verbose output.")
    else:
        log.basicConfig(format="%(levelname)s: %(message)s", level=log.INFO)

    #log.info("This should be verbose.")
    #log.warning("This is a warning.")
    #log.error("This is an error.")
    #log.debug("debug level.")

    # create xml and resources
    s.create_system()
    
if __name__ == '__main__':
    main()
