#!/usr/bin/env python
#coding:utf-8

import os
import sys
import argparse
import textwrap
import collections
import xml.etree.ElementTree as ET
from xml.dom import minidom

try:
    import svgwrite
    from svgwrite import mm
    from svgwrite.container import Group
except:
    print("ERROR: could not find module svgwrite. Install it with: pip install svgwrite")
    quit()
    
import json
from pprint import pprint

# svg -> image conversion
import cairosvg

# eighth of mm
# because rotation argument must be integers
scale_factor = 8

# radius of the light (mm * scale_factor)
light_radius = 1 * scale_factor

# distance between light borders (mm * scale_factor)
light_y_distance = 1 * scale_factor

# distance between light groups (mm * scale_factor)
group_y_distance = 1 * scale_factor

# width of the surrounding white border (mm * scale_factor)
stroke_width = 0.5 * scale_factor
stroke_offset = stroke_width/2.0

# svg document dimensions (to be overwritten by svg_get_total_dimensions)
svg_width = 1
svg_height = 1
    
resources_root_dirname = 'resources/icons'
xml_root_dirname = 'xml/signals'

list_xml_appearance_basename = list()

image_types = {
    "small": {"dirname": "smallschematics", "width": 50},
    "large": {"dirname": "largeschematics", "width": 100}
}

def svg_get_total_dimensions(mast):
    """
    Get total dimensions in mm
    """
    # must be able to modify global variables
    global svg_width
    global svg_height

    svg_width = 2*(light_radius + light_y_distance) + stroke_width
    
    svg_height = 0
    for group in mast['groups']:
        svg_height += light_y_distance
        number_lights = len(group)
        svg_height += number_lights * (light_radius*2 + light_y_distance)

    number_groups = len(mast['groups'])
    svg_height += (number_groups-1) * group_y_distance + stroke_width

    # convert: fraction of mm -> mm
    svg_width = svg_width / scale_factor
    svg_height = svg_height / scale_factor
    
    print("svg_width:  %d mm" % svg_width)
    print("svg_height: %d mm" % svg_height)

def convert_svg_to_image(svg_path, width, image_path):
    """
    Convert svg to image, with specified width
    """
    print("[convert_svg_to_image] %s -> (%d) %s" % (svg_path, width, image_path))
    cairosvg.svg2png(url=svg_path, output_width=width, write_to=image_path)
    
def aspect_is_available(mast, aspect):
    """
    Return whether this mast can show this aspect
    """
    is_able = False

    available_lights = [item for sublist in mast['groups'] for item in sublist]
    needed_lights = [i.replace("flash","") for i in aspect['lit']]
    
    if set(needed_lights) <= set(available_lights):
        is_able = True
    print("available_lights: %s -- needed_lights: %s -> %s" % (available_lights, needed_lights, is_able))
        
    return is_able


def light_group(dwg, group_id, colours, offset, lit=None):
    """
    Generate svg file for a group of lights in a mast. Optional parameter lit defines an aspect.
    """
    colour_translation = {
        "lunar": "white"
    }

    number_lights = len(colours)
    #print "-- number_lights: %d" % number_lights

    y_offset = light_radius + light_y_distance + offset
    y_delta = 2*light_radius + light_y_distance
    x_centre = light_radius + light_y_distance + stroke_offset
    
    rect_width = 2*(light_radius + light_y_distance)
    rect_height = number_lights * (2*light_radius + light_y_distance) + light_y_distance
    #print "rect_height: %d" % rect_height

    # create group
    g = Group(id=group_id)

    # background rectangle
    r = dwg.rect(insert=(0+stroke_offset,offset+stroke_offset),
                 size=(rect_width,rect_height),
                 rx=(light_radius+light_y_distance), ry=(light_radius+light_y_distance),
                 stroke='white', stroke_width=stroke_width)
    g.add(r)

    # convert lit to lit_no_flash
    if lit:
        lit_no_flash = [i.replace("flash","") for i in lit]
    
    # lights
    light = 0
    for colour in colours:
        is_flash = False
        y_centre = y_offset + light*y_delta + stroke_offset
        if colour=="black" or not lit or (lit and colour in lit_no_flash):
            if lit:
                if colour in lit:
                    print("%s: no flash" % colour)
                    is_flash = False
                else:
                    print("%s: flash" % colour)
                    is_flash = True
            
            if colour in colour_translation.keys():
                fill_colour = colour_translation[colour]
            else:
                fill_colour = colour
        else:
            fill_colour = "darkslategray"            

        if is_flash:
            l = Group()
            
            # circle
            c = dwg.circle( center=(x_centre,y_centre), r=light_radius, fill=fill_colour)
            l.add(c)
            
            # rays
            ray_length = 0.5 * scale_factor # (mm * scale_factor)
            ray_width = 0.2 * scale_factor # (mm * scale_factor)
            for angle in range(0, 360, 45):
                #r = dwg.rect(insert=(x_centre,(y_centre+light_radius)), size=(0.1,ray_length), fill=fill_colour, transform="rotate(%d,%d,%d)"%(angle, x_centre, y_centre))
                r = dwg.line(start=(x_centre, y_centre+light_radius),
                             end=(x_centre, y_centre+light_radius+ray_length),
                             stroke=fill_colour, stroke_width=ray_width,
                             transform="rotate(%d,%d,%d)"%(angle, x_centre, y_centre))
                l.add(r)

        else:
            l = dwg.circle( center=(x_centre,y_centre), r=light_radius, fill=fill_colour)
        g.add(l)
        light = light + 1

    dwg.add(g)
        
    offset = rect_height
    return offset

def resources_create_mast(system_name_year, mast):
    """
    Generate svg file for a mast with all its lights activated
    resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>.svg
    """
    svg_mast_path = generate_mast_svg_path(system_name_year, mast)
    print("------------- svg_mast_path: %s" % svg_mast_path)
    
    # init svg drawing
    svg_get_total_dimensions(mast)
    dwg = svgwrite.Drawing(svg_mast_path, profile='full', size=(svg_width*mm, svg_height*mm))
    # set default units as half mm
    dwg.viewbox(0,0,svg_width*scale_factor,svg_height*scale_factor)
    
    # generate each group of lights
    group_offset = 0
    group_id = 0
    for group in mast['groups']:
        group_id_str = "light_group_%d" % group_id
        offset = light_group(dwg, group_id_str, group, group_offset)
        group_offset = offset + group_y_distance
        group_id = group_id + 1

    # save svg to file
    dwg.save(pretty=True)

    # convert to image
    for imagetype in image_types.keys():
        width = image_types[imagetype]['width']
        image_mast_path = generate_mast_image_path(system_name_year, imagetype, mast)
        convert_svg_to_image(svg_mast_path, width, image_mast_path)

def get_number_lights(lists):
    """
    Get the number of active lights. Black do not count.
    """
    number_colours = sum([len(i) for i in lists])
    number_blacks = sum([i.count("black") for i in lists])
    number_lights = number_colours - number_blacks
    
    return number_lights

def generate_mast_svg_path(system_name_year, mast):
    """
    Generate
    resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>.svg
    """
    system_dirname = system_name_year

    number_lights = get_number_lights(mast['groups'])

    svg_mast_dirname = os.path.join(resources_root_dirname, 'svg/aspects', system_dirname)
    if not os.path.exists(svg_mast_dirname):
        os.makedirs(svg_mast_dirname)
    svg_mast_basename = "mast_%s_%d%s_%s.svg" % (mast['type'], number_lights, mast['suffix'], mast['height'])

    svg_mast_path = os.path.join(svg_mast_dirname, svg_mast_basename)
    
    return svg_mast_path

def generate_mast_image_path(system_name_year, imagetype, mast):
    """
    Generate
    resources/icons/<imagetype_dirname>/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>.svg
    """
    system_dirname = system_name_year

    number_lights = get_number_lights(mast['groups'])

    imagetype_dirname = image_types[imagetype]['dirname']

    image_mast_dirname = os.path.join(resources_root_dirname, imagetype_dirname, 'aspects', system_dirname)
    if not os.path.exists(image_mast_dirname):
        os.makedirs(image_mast_dirname)
    image_mast_basename = "mast_%s_%d%s_%s.png" % (mast['type'], number_lights, mast['suffix'], mast['height'])

    image_mast_path = os.path.join(image_mast_dirname, image_mast_basename)
    
    return image_mast_path
    
    
def generate_mast_aspect_image_path(system_name, imagetype, mast, aspect):
    """
    Generate the path for image file
    resources/icons/<imagetype_dirname>/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>/rule-<rule_number>.<gif|png|jpeg>
    """
    system_dirname = system_name
    number_lights = get_number_lights(mast['groups'])
    
    mast_dirname = "%s-%d%s-%s" % (mast['type'], number_lights, mast['suffix'], mast['height'])

    imagetype_dirname = image_types[imagetype]['dirname']
    
    image_mast_aspect_dirname = os.path.join(resources_root_dirname, imagetype_dirname, 'aspects', system_dirname, mast_dirname)
    if not os.path.exists(image_mast_aspect_dirname):
        os.makedirs(image_mast_aspect_dirname)
    #image_mast_aspect_basename = "mast_%s_%d%s_%s-aspect_%s.png" % (mast['type'], number_lights, mast['suffix'], mast['height'], aspect['rule'])
    image_mast_aspect_basename = "rule-%s.png" % (aspect['rule'])

    image_mast_aspect_path = os.path.join(image_mast_aspect_dirname, image_mast_aspect_basename)

    return image_mast_aspect_path

def generate_mast_aspect_svg_path(system_name, mast, aspect):
    """
    Generate the path for svg file
    resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>/rule-<rule_number>.<gif|png|jpeg>
    """
    system_dirname = system_name
    number_lights = get_number_lights(mast['groups'])

    mast_dirname = "%s-%d%s-%s" % (mast['type'], number_lights, mast['suffix'], mast['height'])

    svg_mast_aspect_dirname = os.path.join(resources_root_dirname, 'svg/aspects', system_dirname, mast_dirname)
    if not os.path.exists(svg_mast_aspect_dirname):
        os.makedirs(svg_mast_aspect_dirname)
    svg_mast_aspect_basename = "mast_%s_%d%s_%s-aspect_%s.svg" % (mast['type'], number_lights, mast['suffix'], mast['height'], aspect['rule'])

    svg_mast_aspect_path = os.path.join(svg_mast_aspect_dirname, svg_mast_aspect_basename)

    return svg_mast_aspect_path
    
def resources_create_mast_aspect(system_name, mast, aspect):
    """
    Generate svg file for a mast with lights activated for a given aspect
    resources/icons/svg/aspects/<system_name>-<system_year>/<CPL|PL|SE|SL|>-<number_signal_heads>-<high|low>-<abs|pbs>/rule-<rule_number>.<gif|png|jpeg>
    """    

    svg_mast_aspect_path = generate_mast_aspect_svg_path(system_name, mast, aspect)
    print("------------- svg_mast_aspect_path: %s" % svg_mast_aspect_path)
    
    # init svg drawing
    svg_get_total_dimensions(mast)
    dwg = svgwrite.Drawing(svg_mast_aspect_path, profile='full', size=(svg_width*mm, svg_height*mm))
    # set default units as mm
    dwg.viewbox(0,0,svg_width*scale_factor,svg_height*scale_factor)

    # generate each group of lights
    group_offset = 0
    group_id = 0
    for group in mast['groups']:
        group_id_str = "light_group_%d" % group_id
        offset = light_group(dwg, group_id_str, group, group_offset, aspect['lit'])
        group_offset = offset + group_y_distance
        group_id = group_id + 1

    # save svg to file
    dwg.save(pretty=True)
    
    # convert to image
    for imagetype in image_types.keys():
        width = image_types[imagetype]['width']
        image_mast_aspect_path = generate_mast_aspect_image_path(system_name, imagetype, mast, aspect)
        convert_svg_to_image(svg_mast_aspect_path, width, image_mast_aspect_path)
   
    
def resources_create_masts(system_name_year, masts):
    """
    Generate all svg files for all masts (all lights activated)
    """
    for mast in masts:
        resources_create_mast(system_name_year, mast)

def resources_create_masts_aspects(system_name_year, masts, aspects):
    """
    Generate all svg files for all masts in all aspects (if able)
    """
    for mast in masts:
        for aspect in aspects:
            if aspect_is_available(mast, aspect):
                resources_create_mast_aspect(system_name_year, mast, aspect)

def prettify(elem):
    """
    Return a pretty-printed XML string for the Element.
    """
    rough_string = ET.tostring(elem, 'utf-8')
    reparsed = minidom.parseString(rough_string)
    return reparsed.toprettyxml(indent="  ")

def xml_create_aspects(system):
    """
    Generate xml file with all aspects
    xml/signals/<system_name>/aspects.xml
    """
    system_name_year = "{0:s}-{1:d}".format(system['name'], system['year'])    
    system_dirname = system_name_year

    xml_aspects_dirname = os.path.join(xml_root_dirname, system_dirname)
    if not os.path.exists(xml_aspects_dirname):
        os.makedirs(xml_aspects_dirname)

    xml_aspects_basename = "aspects.xml"
    xml_aspects_path = os.path.join(xml_aspects_dirname, xml_aspects_basename)    

    print("xml_aspects_path: %s" % xml_aspects_path)

    aspecttable = ET.Element('aspecttable',
                             {"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                              "xsi:noNamespaceSchemaLocation": "http://jmri.org/xml/schema/aspecttable.xsd",
                             }
    )

    # name
    name = ET.SubElement(aspecttable, 'name')
    name.text = system['name']

    # date
    date = ET.SubElement(aspecttable, 'date')
    date.text = "%d" % system['year']

    # reference
    reference = ET.SubElement(aspecttable, 'reference')
    reference.text = system['reference']

    # copyright
    copyright = ET.SubElement(aspecttable, 'copyright')
    copyright.set('xmlns','http://docbook.org/ns/docbook')
    for year in system['copyright']['years']:
        year_el = ET.SubElement(copyright, 'year')
        year_el.text = "{0:d}".format(year) 
    holder = ET.SubElement(copyright, 'holder')
    holder.text = system['copyright']['holder']

    # authorgroup
    authorgroup =  ET.SubElement(aspecttable, 'authorgroup')
    authorgroup.set('xmlns','http://docbook.org/ns/docbook')
    for author in system['authorgroup']['authors']:
        author_el = ET.SubElement(authorgroup, 'author')
        personname_el = ET.SubElement(author_el, 'personname')
        firstname_el =  ET.SubElement(personname_el, 'firstname')
        firstname_el.text = author['personname']['firstname']
        surname_el = ET.SubElement(personname_el, 'surname')
        surname_el.text = author['personname']['surname']
        email_el = ET.SubElement(author_el, 'email')
        email_el.text = author['email']

    # revhistory
    revhistory =  ET.SubElement(aspecttable, 'revhistory')
    revhistory.set('xmlns','http://docbook.org/ns/docbook')
    for revision in system['revhistory']['revisions']:
        revision_el = ET.SubElement(revhistory, 'revision')
        revnumber_el = ET.SubElement(revision_el, 'revnumber')
        revnumber_el.text = "{0:d}".format(revision['revnumber'])
        date_el = ET.SubElement(revision_el, 'date')
        date_el.text = revision['date']
        authorinitials_el = ET.SubElement(revision_el, 'authorinitials')
        authorinitials_el.text = revision['authorinitials']
        revremark_el = ET.SubElement(revision_el, 'revremark')
        revremark_el.text = revision['revremark']
        
    # aspects
    aspects = ET.SubElement(aspecttable, 'aspects')
    for aspect in system['aspects']:
        aspect_element = ET.SubElement(aspects, 'aspect')

        # copy all elements, except those declared in ignored_keys
        ignored_keys = ['lit',]
        for (key, value) in aspect.items():
            if key not in ignored_keys:
                element = ET.SubElement(aspect_element, key)
                element.text = aspect[key]

    #imagetypes
    imagetypes = ET.SubElement(aspecttable, 'imagetypes')
    for type in image_types.keys():
        imagetype_element = ET.SubElement(imagetypes, 'imagetype')
        imagetype_element.set('type', type)
    
    #appearancefiles
    appearancefiles = ET.SubElement(aspecttable, 'appearancefiles')
    for appearancefile in list_xml_appearance_basename:
        appearancefile_element = ET.SubElement(appearancefiles, 'appearancefile')
        appearancefile_element.set('href',appearancefile)
        
    #print ET.tostring(top)
    aspecttable_pretty = prettify(aspecttable)
    with open(xml_aspects_path, "w") as f:
        #f.write(aspecttable_pretty.encode('utf-8'))
        f.write(aspecttable_pretty)
    
    #tree = ET.ElementTree(aspecttable)
    #tree.write(open(xml_aspects_path, 'w'), encoding='utf-8')


def xml_create_appearance(system, mast):
    """
    Generate xml file for a given mast, with all the aspects it can show 
    xml/signals/<system_name>/appearance_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.xml
    """
    aspects = system['aspects']
    system_name_year = "{0:s}-{1:d}".format(system['name'], system['year'])
    system_dirname = system_name_year
    
    number_lights = get_number_lights(mast['groups'])

    xml_appearance_dirname = os.path.join(xml_root_dirname, system_dirname)
    if not os.path.exists(xml_appearance_dirname):
        os.makedirs(xml_appearance_dirname)

    xml_appearance_basename = "appearance-%s_%d%s_%s.xml" % (mast['type'], number_lights, mast['suffix'], mast['height'])
    xml_appearance_path = os.path.join(xml_appearance_dirname, xml_appearance_basename)    

    print("xml_appearance_path: %s" % xml_appearance_path)
    
    appearancetable = ET.Element('appearancetable',
                             {"xmlns:xsi": "http://www.w3.org/2001/XMLSchema-instance",
                              "xsi:noNamespaceSchemaLocation": "http://jmri.org/xml/schema/appearancetable.xsd",
                             }
    )
    
    # copyright
    copyright = ET.SubElement(appearancetable, 'copyright')
    copyright.set('xmlns','http://docbook.org/ns/docbook')
    for year in system['copyright']['years']:
        year_el = ET.SubElement(copyright, 'year')
        year_el.text = "{0:d}".format(year) 
    holder = ET.SubElement(copyright, 'holder')
    holder.text = system['copyright']['holder']

    # authorgroup
    authorgroup =  ET.SubElement(appearancetable, 'authorgroup')
    authorgroup.set('xmlns','http://docbook.org/ns/docbook')
    for author in system['authorgroup']['authors']:
        author_el = ET.SubElement(authorgroup, 'author')
        personname_el = ET.SubElement(author_el, 'personname')
        firstname_el =  ET.SubElement(personname_el, 'firstname')
        firstname_el.text = author['personname']['firstname']
        surname_el = ET.SubElement(personname_el, 'surname')
        surname_el.text = author['personname']['surname']
        email_el = ET.SubElement(author_el, 'email')
        email_el.text = author['email']

    # revhistory
    revhistory =  ET.SubElement(appearancetable, 'revhistory')
    revhistory.set('xmlns','http://docbook.org/ns/docbook')
    for revision in system['revhistory']['revisions']:
        revision_el = ET.SubElement(revhistory, 'revision')
        revnumber_el = ET.SubElement(revision_el, 'revnumber')
        revnumber_el.text = "{0:d}".format(revision['revnumber'])
        date_el = ET.SubElement(revision_el, 'date')
        date_el.text = revision['date']
        authorinitials_el = ET.SubElement(revision_el, 'authorinitials')
        authorinitials_el.text = revision['authorinitials']
        revremark_el = ET.SubElement(revision_el, 'revremark')
        revremark_el.text = revision['revremark']

    # aspecttable
    aspecttable = ET.SubElement(appearancetable, 'aspecttable')
    aspecttable.text = system_name_year

    # name
    name = ET.SubElement(appearancetable, 'name')
    name.text = mast['name']

    # reference
    reference = ET.SubElement(appearancetable, 'reference')
    reference.text = mast['reference']
    
    # description
    description = ET.SubElement(appearancetable, 'description')
    description.text = mast['description']
    
    # appearances
    appearances = ET.SubElement(appearancetable, 'appearances')
    for aspect in aspects:
        if aspect_is_available(mast, aspect):
            aspect_element = ET.SubElement(appearances, 'appearance')

            aspectname = ET.SubElement(aspect_element, 'aspectname')
            aspectname.text = aspect['name']
            
            for show in aspect['lit']:
                show_element = ET.SubElement(aspect_element, 'show')
                show_element.text = show

            reference = ET.SubElement(aspect_element, 'reference')
            reference.text = aspect['rule']

            for imagetype in image_types.keys():
                imagelink = ET.SubElement(aspect_element, 'imagelink')
                imagelink.text = os.path.join( '../../..', generate_mast_aspect_image_path(system_name_year, imagetype, mast, aspect) )
                imagelink.set('type',imagetype)
                

    appearancetable_pretty = prettify(appearancetable)
    with open(xml_appearance_path, "w") as f:
        #f.write(appearancetable_pretty.encode('utf-8'))
        f.write(appearancetable_pretty)

    return xml_appearance_basename

    #print prettify(appearancetable)
    #tree = ET.ElementTree(appearancetable)
    #tree.write(open(xml_appearance_path, 'w'), encoding='utf-8')
    
def xml_create_appearances(system):
    """
    Generate all xml files for all masts
    """
    #system_name_year = "%s-%s" % (system['name'], system['year'])
    for mast in system['masts']:
        xml_appearance_basename = xml_create_appearance(system, mast)
        list_xml_appearance_basename.append(xml_appearance_basename)

    print(list_xml_appearance_basename)
        
def create_system(system):
    """
    Generate all svg and xml files
    """
    system_name_year = "{0:s}-{1:d}".format(system['name'], system['year'])

    # several svg/image files for all masts
    resources_create_masts(system_name_year, system['masts'])
    
    # several svg/image files for all masts
    resources_create_masts_aspects(system_name_year, system['masts'], system['aspects'])

    # several xml files for all masts (appearance_<mast>.xml)
    xml_create_appearances(system)

    # one xml file for aspects (aspects.xml)
    xml_create_aspects(system)


def main():
    # arguments
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description=textwrap.dedent('''\
                                     Generate files for JMRI signals: http://jmri.org/help/en/html/tools/signaling/DefineNewAspects.shtml

                                     Created directories and files:
                                     - xml/signals/<system_name>-<year>/
                                         - aspects.xml
                                         - appearance-<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.xml
                                     - resources/icons/
                                         - (not used by jmri) svg/aspects/<system_name>-<year>/
                                             - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.svg
                                             - <CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>/
                                                 - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>-aspect_<aspect>.svg
                                         - <largeschematics|smallschematics>/aspects/<system_name>-<year>/
                                             - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.png
                                             - <CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>/
                                                 - rule-<aspect>.png
                                     '''))
    parser.add_argument('json_file', 
                        help='json file (see examples in json/ dir)')
    args = parser.parse_args()

    # read json file (order must be maintained)
    json_file = args.json_file
    with open(json_file) as data_file:    
        system = json.load(data_file, object_pairs_hook=collections.OrderedDict)
    #pprint(system)
    
    create_system(system)

        
if __name__ == '__main__':
    main()
