# JMRI signals #

See: [JMRI: Defining Your Own Signaling System](http://jmri.org/help/en/html/tools/signaling/DefineNewAspects.shtml)

Can be used with hardware decoders with [NmraDccAccessoryDecoder_2_fpm](https://gitlab.com/dcc-fpm/nmradcc-fpm/-/tree/master/NmraDccAccessoryDecoder_2_fpm)

## Installation ##

### Using Python 3 ###

NOTE: Python 2.7 is not compatible with latest cairosvg package. Use Python 3.

1. Install package to create Python virtualenvs:
    - Mageia: `sudo urpmi python3-virtualenv`
2. Create virtualenv dir: 
```
virtualenv env
```
3. activate it:
```
source env/bin/activate
```
4. update pip:
```
pip install --upgrade pip
```
5. install modules (listed in pip requirements):
```
pip install -r pip_requirements.txt
```

## Usage ##

```
python signals.py --help
python signals.py json/adif.json
```

Created directories and files:
```
- xml/signals/<system_name>-<year>/
    - aspects.xml
    - appearance-<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.xml
- resources/icons/
    - (not used by jmri) svg/aspects/<system_name>-<year>/
        - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.svg
        - <CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>/
            - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>-aspect_<aspect>.svg
    - <largeschematics|smallschematics>/aspects/<system_name>-<year>/
        - mast_<CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>.png
        - <CPL|PL|SE|SL|>_<number_signal_heads>_<high|low>_<abs|pbs>/
            - rule-<aspect>.png
```

Example of created files:
```
...
```

## Install generated files in JMRI ##

Xml files:
```
cd ~/src/JMRI/xml/signals/
ln -s ~/src/jmri-signals/xml/signals/ADIF-2018 .
```

Resources files:
```
cd ~/src/JMRI/resources/icons/
ln -s ~/src/jmri-signals/resources/icons/smallschematics/aspects/ADIF-2018/ smallschematics/aspects/
ln -s ~/src/jmri-signals/resources/icons/largeschematics/aspects/ADIF-2018/ largeschematics/aspects/
```
